# OpenML dataset: datasets-uta4

https://www.openml.org/d/42349

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Francisco Maria Calisto, Nuno Jardim Nunes, Jacinto Carlos Nascimento  

This work was partially supported by national funds through FCT and IST through the UID/EEA/50009/2013 project", "BL89/2017-IST-ID grant.  

**Source**: [original](https://www.kaggle.com/MIMBCD-UI/uta4-sm-vs-mm-sheets-nameless) - 03/03/2018  

**Please cite**: https://doi.org/10.1145/3399715.3399744

In this dataset, we present usability (SUS), workload (NASA-TLX), time, and rates (BIRADS) results of clinicians from our User Tests and Analysis 4 (UTA4) study.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42349) of an [OpenML dataset](https://www.openml.org/d/42349). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42349/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42349/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42349/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

